package com.company;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "seats")

public class Seat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "seatId")
    private Integer seatId;

    @Column(name = "rowSeat")
    private Integer rowSeat;

    @Column(name = "number")
    private Integer number;

    @ManyToOne
    @JoinColumn(name = "roomId")
    private Room room;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "reservationSeat",
            joinColumns = @JoinColumn(name = "seatId"),
            inverseJoinColumns = @JoinColumn(name = "reservationId")
    )
    private Set<Reservation> reservations = new HashSet<Reservation>();

    @OneToMany(mappedBy = "seat")
    private Set<Ticket> tickets = new HashSet<Ticket>();

    public Integer getSeatId() {
        return seatId;
    }

    public void setSeatId(Integer seatId) {
        this.seatId = seatId;
    }

    public Integer getRowSeat() {
        return rowSeat;
    }

    public void setRowSeat(Integer rowSeat) {
        this.rowSeat = rowSeat;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Set<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public String toString(){
        return "Seat id: " + seatId + ", row: " + rowSeat + ", number: " + number + ", room: " + room
                + "\n";
    }
}
