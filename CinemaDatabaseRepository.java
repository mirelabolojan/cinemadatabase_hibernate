package com.company;

import org.hibernate.Session;
import org.hibernate.query.Query;


import java.util.List;

public class CinemaDatabaseRepository {

    public static List<Movie> listAllMovies(){
        Session session = HibernateUtils.getSessionFactory().openSession();

        String listAllMoviesHQL = "from Movie";
        Query listAllMoviesQuery = session.createQuery(listAllMoviesHQL);

        List<Movie> movies = listAllMoviesQuery.list();

        session.close();

        return movies;
    }

    public static List<Movie> listAllMoviesByDate(String dateTime){
        Session session = HibernateUtils.getSessionFactory().openSession();

        String listAllMoviesByDateHQL = "select movie from Schedule where startTime = :time";
        Query listAllMoviesQuery = session.createQuery(listAllMoviesByDateHQL);
        listAllMoviesQuery.setParameter("time", dateTime);

        List<Movie> listMovies = listAllMoviesQuery.list();

        session.close();

        return listMovies;
    }

    public static List<Movie> listAllMoviesNotScheduled(){
        Session session = HibernateUtils.getSessionFactory().openSession();

        String listAllMoviesNotScheduledHQL = "from Movie where movieId not in (select movie from Schedule)";
        Query listAllMovieQuery = session.createQuery(listAllMoviesNotScheduledHQL);

        List<Movie> movies = listAllMovieQuery.list();

        session.close();

        return movies;
    }

    public static List<Movie> listAllMoviesReservedByClient(Client client){
        Session session = HibernateUtils.getSessionFactory().openSession();

        String listAlMoviesHQL = "from Movie where movieId in (select movie from Schedule where scheduleId in (select schedule from Reservation where clientId in (select clientId from Client where firstName = :firstName and lastName = :lastName)))";
        Query listAllMoviesQuery = session.createQuery(listAlMoviesHQL);
        listAllMoviesQuery.setParameter("firstName", client.getFirstName());
        listAllMoviesQuery.setParameter("lastName", client.getLastName());

        List<Movie> movies = listAllMoviesQuery.list();

        session.close();

        return movies;
    }

    public static List<Seat> listAllSeatRowAndNumberByClient(Client client){
        Session session = HibernateUtils.getSessionFactory().openSession();

        String listAllSeatsHQL = "select rowSeat, number from Seat where seatId in (select reservationId from Reservation where client in (select clientId from Client where firstName = :firstName and lastName = :lastName))";
        Query listAllSeatsQuery = session.createQuery(listAllSeatsHQL);
        listAllSeatsQuery.setParameter("firstName", client.getFirstName());
        listAllSeatsQuery.setParameter("lastName", client.getLastName());

        List<Seat> seats = listAllSeatsQuery.list();

        session.close();

        return seats;
    }

    public static List<Ticket> listAllTicketsSoldForMovie(Movie movie){
        Session session = HibernateUtils.getSessionFactory().openSession();

        String allMovie = "from Ticket where schedule in (select scheduleId from Schedule where movie in (select movieId from Movie where name = :name))";
        Query allMovieQuery = session.createQuery(allMovie);
        allMovieQuery.setParameter("name", movie.getName());

        List<Ticket> tickets = allMovieQuery.list();

        session.close();

        return tickets;
    }
}
