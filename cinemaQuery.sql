create database cinema_db;

create table movies (movieId int auto_increment primary key, name varchar(45), category varchar(45), durationInMinutes int(11), description varchar(255));

create table clients (clientId int auto_increment primary key, firstName varchar(45), lastName varchar(45), email varchar(20), dateOfBirth date);

create table ticketCategories (ticketCategoryId int auto_increment primary key, type varchar(45), price int);

create table rooms (roomId int auto_increment primary key, number int, maxSeats int, location varchar(45));

create table schedules (scheduleId int auto_increment primary key, startTime datetime, movieId int, roomId int, 
constraint fk_schedule_movie foreign key (movieId) references movies(movieId), 
constraint fk_schedule_room foreign key (roomId) references rooms(roomId));

create table reservations (reservationId int auto_increment primary key, isPaid int, clientId int, scheduleId int,
constraint fk_reservation_client foreign key (clientId) references clients(clientId),
constraint fk_reservation_schedule foreign key (scheduleId) references schedules(scheduleId) );

create table seats (seatId int auto_increment primary key, rowSeat int, number int, roomId int,
constraint fk_seat_room foreign key (roomId) references rooms(roomId) );

create table reservationSeat (reservationSeatId int auto_increment primary key, reservationId int, seatId int,
constraint fk_reservationSeat_reservation foreign key (reservationId) references reservations(reservationId),
constraint fk_reservationSeat_seat foreign key (seatId) references seats(seatId) );

create table tickets (ticketId int auto_increment primary key, scheduleId int, seatId int, categoryId int,
constraint fk_ticket_schedule foreign key (scheduleId) references schedules(scheduleId),
constraint fk_ticket_seat foreign key (seatId) references seats(seatId),
constraint fk_ticket_category foreign key(categoryId) references ticketcategories(ticketCategoryId) );

insert into movies (name, category, durationInMinutes, description) values 
 ('Arrow', 'Drama, Aventura', '155', 'Călătoria mitică și emoționantă din „Dune” 
 spune povestea lui Paul Atreides, un tânăr strălucit și talentat, născut într-un mare destin, dincolo de înțelegerea sa.'),
 ('The Piano', 'Drama, Romantic', '160', 'Acțiunea se desfășoară în Noua Zeelanda a mijlocului
  de secol nouăsprezece, unde o tânără femeie scoțiana, mută, sosește împreună cu fiica sa pentru a se căsători.'),
 ('Forrest Gump', 'Comedie, Drama', '125', 'Povestea începe cu o pană căzând la 
 piciorul lui Forrest. Forrest o ia și o pune în geanta sa. Așezat pe o bancă, așteptând autobuzul, Forrest, pe parcursul filmului, 
 își povestește viașa care este aidoma penei luate de vânt.'), 
 ('Titanic', 'Drama, Romantic', '130', '"Titanicul" lui James Cameron este o poveste de 
dragoste îmbinată cu scene de acțiune, care are loc în timpul primei călătorii a navei Titanic, mândria companiei White Star Line și cel mai mare obiect în 
mișcare care fusese vreodată construit.');

insert into clients (firstName, lastName, email, dateOfBirth) values 
('Denisa', 'Sabau', 'denisas@gmail.com', '1999-03-01'),
('Mirela', 'Bolojan', 'mirela@gmail.com', '1993-06-16'),
('Denis', 'Bolojan', 'denis@gmail.com', '1998-02-26'),
('Cristina', 'Vacaru', 'cmv@gmail.com', '1992-10-08');

insert into rooms (number, maxSeats, location) values 
('1', '60', 'Ground Floor'),
('2', '80', 'Ground Floor'),
('3', '110', 'First Floor'),
('4', '100', 'First Floor');

insert into schedules (startTime, movieId, roomId) values 
('2021-12-20 12:30:00', 1, 2),
('2021-12-20 12:30:00', 2, 3),
('2021-12-21 12:30:00', 2, 2),
('2021-12-21 16:30:00', 3, 1);

insert into reservations (isPaid, clientId, scheduleId) values 
(20, 1, 1),
(15, 2, 2),
(0, 3, 3),
(25, 4, 3),
(50, 2, 4);

insert into seats (rowSeat, number, roomId) values 
(1,1,1), (1,2,1), (1,3,1),
(1,1,2), (1,2,2), (1,3,2),
(1,1,3), (1,2,3), (1,3,3),
(1,1,4), (1,2,4), (1,3,4);

insert into reservationseat (reservationId, seatId) values (1,1), (1,2), (1,3), (2,4), (3,5);

insert into ticketcategories (type, price) values ('Adult', 50), ('Children', 25), ('Student', 35);

insert into tickets (scheduleId, seatId, categoryId) values (1, 1, 2), (1,2,3),(2,3,1);

-- list all movies that are not scheduled yet
select * from movies where movieId not in (select movieId from schedules);

-- list all movies reserved by Mirela Bolojan
select * from movies where movieId in (select movieId from schedules where scheduleId in 
(select scheduleId from reservations where clientId in 
(select clientId from clients where firstName = 'Mirela' and lastName = 'Bolojan'))); 

select * from reservationseat inner join seats, reservations where seats.seatId = reservationseat.seatId and reservations.reservationId = reservationseat.reservationId ;

select rowSeat, number from seats where seatId in (select seatId from reservationseat where reservationId in(select reservationId from reservations where clientId in
	(select clientId from clients where firstName = 'Denis' and lastName = 'Bolojan')));
    
