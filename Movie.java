package com.company;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "movies")

public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "movieId")
    private Integer movieId;

    @Column(name = "name")
    private String name;

    @Column(name = "category")
    private String category;

    @Column(name = "durationInMinutes")
    private Integer durationInMinutes;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "movie")
    private Set<Schedule> schedules = new HashSet<Schedule>();

    public Integer getMovieId(){
        return movieId;
    }

    public void setMovieId(Integer movieId){
        this.movieId = movieId;
    }

    public String getName(){
        return  name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getDurationInMinutes() {
        return durationInMinutes;
    }

    public void setDurationInMinutes(Integer durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(Set<Schedule> schedules) {
        this.schedules = schedules;
    }

    @Override
    public String toString(){
        return "Movie ID: " + movieId + ", name: " + name + ", category: " + category + ", duration in minutes: "
                + durationInMinutes + ", description: " + description + "\n";
    }
}
