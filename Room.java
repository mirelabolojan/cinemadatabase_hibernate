package com.company;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "rooms")

public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "roomId")
    private Integer roomId;

    @Column(name = "number")
    private Integer number;

    @Column(name = "maxSeats")
    private Integer maxSeats;

    @Column(name = "location")
    private String location;

    @OneToMany(mappedBy = "room")
    private Set<Schedule> schedules = new HashSet<Schedule>();

    @OneToMany(mappedBy = "room")
    private Set<Seat> seats = new HashSet<Seat>();

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(Integer maxSeats) {
        this.maxSeats = maxSeats;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Set<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(Set<Schedule> schedules) {
        this.schedules = schedules;
    }

    public Set<Seat> getSeats() {
        return seats;
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "Room Id: " + roomId + ", number: " + number + ", max seats: " + maxSeats + ", location: " + location
                + "\n";
    }
}
