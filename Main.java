package com.company;


import static com.company.CinemaDatabaseRepository.*;

public class Main {

    public static void main(String[] args) {

//        List all movies
        System.out.println(listAllMovies());
        System.out.println("**********************");

//        List all movies by date
        System.out.println(listAllMoviesByDate("2021-12-21 16:30:00"));
        System.out.println("**********************");

//        List all movies that are not scheduled
        System.out.println(listAllMoviesNotScheduled());
        System.out.println("**********************");

//        list all movies reserved by a client
        Client client = new Client();
        client.setFirstName("Mirela");
        client.setLastName("Bolojan");
        System.out.println(listAllMoviesReservedByClient(client));
        System.out.println("**********************");

//        list seats reserved by client
        Client client1 = new Client();
        client1.setFirstName("Mirela");
        client1.setLastName("Bolojan");
        System.out.println(listAllSeatRowAndNumberByClient(client1));
        System.out.println("************************");

//        list tickets sold to a movie
        Movie movie = new Movie();
        movie.setName("The Piano");
        System.out.println(listAllTicketsSoldForMovie(movie));
        System.out.println("*************************");

    }
}
