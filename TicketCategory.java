package com.company;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ticketcategories")

public class TicketCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ticketCategoryId")
    private Integer ticketCategoryId;

    @Column(name = "type")
    private String type;

    @Column(name = "price")
    private Integer price;

    @OneToMany(mappedBy = "ticketCategory")
    private Set<Ticket> tickets = new HashSet<Ticket>();

    public Integer getTicketCategoryId() {
        return ticketCategoryId;
    }

    public void setTicketCategoryId(Integer ticketCategoryId) {
        this.ticketCategoryId = ticketCategoryId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public String toString(){
        return "Ticket category id: " + ticketCategoryId + ", type: " + type + ", price: " + price
                + "\n" ;
    }

}
